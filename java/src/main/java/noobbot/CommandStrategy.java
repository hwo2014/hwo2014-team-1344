package noobbot;


import noobbot.commands.SendMsg;

public interface CommandStrategy {
    SendMsg calcCommand();
}
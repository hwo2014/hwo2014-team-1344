package noobbot;

import com.google.common.base.Stopwatch;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import noobbot.commands.*;
import noobbot.model.CarId;
import noobbot.model.CarPosition;
import noobbot.model.GameData;
import noobbot.model.Piece;

import java.io.*;
import java.lang.reflect.Type;
import java.net.Socket;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Main {
    private static final boolean DEBUG = Boolean.valueOf(System.getProperty("debug"));

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        //SendMsg join = JoinRace.createJoinUSA(Long.valueOf(1));
        //SendMsg join = JoinRace.createJoinFrance(Long.valueOf(1));
        //SendMsg join = JoinRace.createJoinGermany(Long.valueOf(1));
        //new Main(reader, writer, join);
        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    private GameData model;
    private CarId myCar;

    String fd(double d) {
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.GERMAN);
        ((DecimalFormat) numberFormat).applyPattern("+#,##0.00;-##,##0.00");
        return numberFormat.format(new Double(d));
    }

    public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        while ((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            String json = msgFromServer.data != null ? msgFromServer.data.toString() : "";
            if (msgFromServer.msgType.equals("carPositions")) {
                Type listType = new TypeToken<ArrayList<CarPosition>>() {
                }.getType();
                List<CarPosition> positions = gson.fromJson(json, listType);
                model.setCurrentPositions(positions);
                CommandStrategy command = new SimpleSpeedCommandStrategy(model);
                Stopwatch sw = Stopwatch.createStarted();
                SendMsg msg = command.calcCommand();
                send(msg);
                if (DEBUG) debug(msg);
                long elapsed = sw.elapsed(TimeUnit.MILLISECONDS);
                if(elapsed>500){
                    System.out.print("elased " + elapsed + " -- ");
                }
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("yourCar")) {
                myCar = gson.fromJson(json, CarId.class);
                System.out.println("my car " + myCar.getName() + " " + myCar.getColor());
            } else if (msgFromServer.msgType.equals("gameInit")) {
                model = gson.fromJson(json, GameData.class);
                model.init();
                model.markMyCar(myCar);
                System.out.println("Race init: count cars " + model.getRace().getCars().size());
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("turboAvailable")) {
                System.out.println("turboAvailable");
                model.setTuroAvailable(true);
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else if (msgFromServer.msgType.equals("lapFinished")) {
                System.out.println("lapFinished " + msgFromServer.msgType + "" + json);
            } else {
                System.out.println("Message from Server " + msgFromServer.msgType + "" + json);
                send(new Ping());
            }
        }

    }

    private int tick;
    private void debug(SendMsg msg) {
        CarPosition myCurrentPositions = model.getMyCurrentPosition();
        Piece nextBendPiece = model.nextBendPiece();
        long pieceIndex = myCurrentPositions.getPiecePosition().getPieceIndex();
        long currentRadius = (long) model.getCurrentPiece().getRadius();

        String x =
                + tick++
                        + " i " + pieceIndex
                        + " spd " + fd(model.calcCurrentSpeed())
                        + " ang " + fd(myCurrentPositions.getAngle())
                        + " cmd " + msg.toString()
                        + " ad " + fd(model.calcAngleDiff())
                        + " acc " + fd(model.calcAcceleration())
                        + " cbend " + fd(model.getCurrentPiece().getAngle()) + "(" + currentRadius + ")"
                        + " nbend " + fd(nextBendPiece.getAngle()) + "(" + model.calcNextBendDistance() + "/" + fd(nextBendPiece.getRadius()) + ")"
                        + " dist " + fd(myCurrentPositions.getPiecePosition().getInPieceDistance())
                        + " pl " + fd(model.getCurrentPiece().getLength())
                        + " csl " + fd(model.calcCurrentSectionLength())
                        + " cspos " + fd(model.calcCurrentSectionPosition())
                        + " cs% " + fd(model.calcCurrentSectionPositionPercentage())
                        + " lap " +model.getMyCurrentPosition().getLap();
        System.out.println(x);
    }

    private void send(final SendMsg msg) {
        String json = msg.toJson();
        writer.println(json);
        writer.flush();
    }
}



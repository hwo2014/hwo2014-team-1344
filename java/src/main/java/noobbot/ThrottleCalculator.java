package noobbot;

import com.google.common.base.Optional;
import noobbot.commands.Throttle;


/**
 * Created by jonas on 27.04.14.
 */
public interface ThrottleCalculator {
    Optional<Throttle> calculate();
}

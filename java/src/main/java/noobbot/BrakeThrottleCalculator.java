package noobbot;

import com.google.common.base.Optional;
import noobbot.commands.Throttle;
import noobbot.model.*;

import java.util.Objects;

/**
 * Created by jonas on 27.04.14.
 */
public class BrakeThrottleCalculator implements ThrottleCalculator{

    private GameData model;
    private TrackProfile trackProfile;

    public BrakeThrottleCalculator(GameData model){
        this.model = Objects.requireNonNull(model);
        this.trackProfile = model.getTrackProfile();
    }

    public Optional<Throttle> calculate() {
        if (model.getCurrentPiece().isBend()) {
            return Optional.absent();
        }
        Throttle throttle = null;

        Piece nextBend = trackProfile.getNextBend(model.getMyCurrentPosition().getPieceIndex());
        TrackItem nextBendItem = trackProfile.getTrackItemBy(nextBend.getIndex());

        BendCategoryCalculator categoryCalculator = new BendCategoryCalculator();
        BendCategory category = categoryCalculator.calcCategory(nextBendItem);

        //System.out.print(category);
        double carAngle = model.getMyCurrentPosition().getAngle();
        double inPosPercentage = model.calcCurrentSectionPositionPercentage();
        double currentSpeed = model.calcCurrentSpeed();
        int nextBendDistance = model.calcNextBendDistance();

        if (category == BendCategory.VERY_SOFT &&
                currentSpeed > 9 &&
                inPosPercentage > 80) {
            throttle = createThrottle();
        }else if( category == BendCategory.SOFT &&
                currentSpeed > 8.5 &&
                inPosPercentage >= 70){
            throttle = createThrottle();
        }else if(category == BendCategory.HARD &&
                currentSpeed > 7 &&
                nextBendDistance <3){
            throttle = createThrottle();
        }else if(category == BendCategory.VERY_HARD &&
                currentSpeed > 5 &&
                nextBendDistance < 3){
            throttle = createThrottle();
        }

        if(nextBendDistance<9 && currentSpeed>10){
            throttle = createThrottle();
        }

        if(carAngle>35 && nextBendDistance<2){
            throttle = createThrottle();
        }

        return Optional.fromNullable(throttle);
    }

    private Throttle createThrottle() {
        return new Throttle(0.1, model.getGameTick());
    }
}

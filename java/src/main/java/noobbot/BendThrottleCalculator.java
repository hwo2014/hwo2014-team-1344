package noobbot;

import com.google.common.base.Optional;
import noobbot.commands.Throttle;
import noobbot.model.*;

import java.util.Objects;

/**
 * Created by jonas on 28.04.14.
 */
public class BendThrottleCalculator implements ThrottleCalculator {

    private GameData model;

    public BendThrottleCalculator(GameData model){
        this.model = Objects.requireNonNull(model);
    }

    @Override
    public Optional<Throttle> calculate() {
        Piece currentPiece = model.getCurrentPiece();
        if (!currentPiece.isBend()) {
            return Optional.absent();
        }

        double currentSpeed = model.calcCurrentSpeed();
        double inPosPercentage = model.calcCurrentSectionPositionPercentage();
        int nextBendDistance = model.calcNextBendDistance();
        double carAngle = Math.abs(model.getMyCurrentPosition().getAngle());
        double angleDiff = Math.abs(model.calcAngleDiff());

        TrackItem currentBend = model.getTrackProfile().getTrackItemBy(currentPiece.getIndex());


        int nextBendIndex = model.getTrackProfile().getNextBend(currentPiece.getIndex()).getIndex();
        TrackItem nextBend = model.getTrackProfile().getTrackItemBy(nextBendIndex);


        BendCategory category = new BendCategoryCalculator().calcCategory(currentBend);
        double maxSpeed = calcMaxSpeed(category);

        BendCategory categoryNextBend = new BendCategoryCalculator().calcCategory(nextBend);
        double maxSpeedNextBend = calcMaxSpeed(categoryNextBend);

        Throttle throttle = null;
        if(inPosPercentage<25 && currentSpeed >= maxSpeed){
            throttle = createThrottle();
        }

        if(angleDiff>2.5 && inPosPercentage<70){
            throttle = createThrottle();
        }

        if(carAngle>25 && inPosPercentage<80){
            throttle = createThrottle();
        }

        if (nextBendDistance < 3 &&
                inPosPercentage > 70 &&
                (carAngle > 15 || currentSpeed >= maxSpeedNextBend)) {
            throttle = createThrottle();
        }

        return Optional.fromNullable(throttle);
    }

    private double calcMaxSpeed(BendCategory category) {
        double maxSpeed = 0;
        switch (category){
            case VERY_SOFT: maxSpeed = 10;
                break;
            case SOFT: maxSpeed = 8;
                break;
            case HARD: maxSpeed = 4.5;
                break;
            case VERY_HARD: maxSpeed = 4;
                break;
        }
        return maxSpeed;
    }

    private Throttle createThrottle() {
        return new Throttle(0.0, model.getGameTick());
    }
}

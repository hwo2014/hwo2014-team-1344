package noobbot;

import noobbot.model.BendCategory;
import noobbot.model.Piece;
import noobbot.model.TrackItem;

import java.util.List;

/**
 * Created by jonas on 27.04.14.
 */
public class BendCategoryCalculator {

    public BendCategory calcCategory(TrackItem item) {
        BendCategory category = BendCategory.HARD;

        List<Piece> pieces = item.getPieces();

        Piece firstPiece = item.getFirstPiece();

        double angle = Math.abs(firstPiece.getAngle());
        double radius = firstPiece.getRadius();

        if (pieces.size() == 1) {
            if (angle < 25) {
                category = BendCategory.VERY_SOFT;
            } else {
                category = BendCategory.SOFT;
            }
        } else if (angle <= 25) {
            category = BendCategory.SOFT;
        } else if (angle > 25) {
            if (radius >= 100) {
                category = BendCategory.HARD;
            } else {
                category = BendCategory.VERY_HARD;
            }
        }
        return category;
    }
}

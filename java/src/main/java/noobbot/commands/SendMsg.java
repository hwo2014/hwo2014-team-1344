package noobbot.commands;

import com.google.gson.Gson;
import noobbot.commands.MsgWrapper;

public abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

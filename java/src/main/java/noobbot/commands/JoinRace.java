package noobbot.commands;

/**
 * Created by jonas on 19.04.14.
 */
public class JoinRace extends SendMsg {

    private Botid botId;

    public String trackName;
    public Long carCount;

    public JoinRace(final String name, final String key, final Long carCount, String trackName) {
        this.botId = new Botid();
        this.botId.name = name;
        this.botId.key = key;
        this.trackName = trackName;
        this.carCount = carCount;
    }

    public static JoinRace createJoinUSA(Long carCount) {
        return new JoinRace("Koenigsklasse","EvGxOSTJhc6K5w", carCount, "usa");
    }

    public static JoinRace createJoinGermany(Long carCount) {
        return new JoinRace("Koenigsklasse","EvGxOSTJhc6K5w", carCount, "germany");
    }

    public static JoinRace createJoinFrance(Long carCount) {
        return new JoinRace("Koenigsklasse","EvGxOSTJhc6K5w", carCount, "france");
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}

class Botid{
    public String name;
    public String key;
}
package noobbot.commands;

/**
 * {"msgType": "turbo", "data": "Pow pow pow pow pow, or your of personalized turbo message"}
 * Created by jonas on 27.04.14.
 */
public class Turbo extends SendMsg {

    private String msg;

    public Turbo(String msg) {
        this.msg = msg;
    }

    @Override
    protected Object msgData() {
        return msg;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }

    @Override
    public String toString() {
        return "Turbo";
    }
}

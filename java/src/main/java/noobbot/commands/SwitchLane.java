package noobbot.commands;

/**
 * Created by jonas on 16.04.14.
 */
public final class SwitchLane extends SendMsg {

    private String direction;

    private SwitchLane(String direction) {
        this.direction = direction;
    }

    public static SwitchLane createLeft(){
        return new SwitchLane("Left");
    }

    public static SwitchLane createRight(){
        return new SwitchLane("Right");
    }

    @Override
    protected Object msgData() {
        return direction;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }

    @Override
    public String toString() {
        return "Switch Lane to " + direction;
    }
}

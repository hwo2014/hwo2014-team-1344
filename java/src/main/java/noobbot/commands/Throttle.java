package noobbot.commands;

public class Throttle extends SendMsg {
    private double data;
    private long gameTick;

    public Throttle(double data, long gameTick) {
        this.data = data;
        this.gameTick = gameTick;
    }

    @Override
    public String toString() {
        return "Throttle " + data;
    }

    @Override
    protected Object msgData() {
        return data;
    }

    protected Object gameTick() {
        return gameTick;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

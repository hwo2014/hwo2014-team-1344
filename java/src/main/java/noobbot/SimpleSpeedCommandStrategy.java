package noobbot;

import com.google.common.base.Optional;
import noobbot.commands.SendMsg;
import noobbot.commands.SwitchLane;
import noobbot.commands.Throttle;
import noobbot.commands.Turbo;
import noobbot.model.GameData;
import noobbot.model.Piece;

import static java.util.Objects.requireNonNull;

/**
 * Created by jonas on 18.04.14.
 */
public class SimpleSpeedCommandStrategy implements CommandStrategy {

    private final GameData gameData;

    public SimpleSpeedCommandStrategy(GameData data) {
        gameData = requireNonNull(data);
    }

    @Override
    public SendMsg calcCommand() {

        SendMsg switchCommand = calcSwitch();
        if (switchCommand != null) {
            return switchCommand;
        }

        BrakeThrottleCalculator brakeCalc = new BrakeThrottleCalculator(gameData);
        Optional<Throttle> brake = brakeCalc.calculate();
        if (brake.isPresent()) {
            return brake.get();
        }

        BendThrottleCalculator bendCalc = new BendThrottleCalculator(gameData);
        Optional<Throttle> bend = bendCalc.calculate();
        if (bend.isPresent()) {
            return bend.get();
        }

        Optional<Turbo> turboCommand = calcSpecialCasesCommand();
        if (turboCommand.isPresent()) {
            return turboCommand.get();
        }

        // sonst vollgas
        return new Throttle(1.0, gameData.getGameTick());
    }


    private Optional<Turbo> calcSpecialCasesCommand() {

        Piece currentPiece = gameData.getCurrentPiece();
        double bendAngle = Math.abs(gameData.getCurrentPiece().getAngle());
        int nextBendDistance = gameData.calcNextBendDistance();

        Turbo turbo = null;

        if (gameData.hasTurbo() &&
                nextBendDistance > 5 &&
                bendAngle == 0) {
            gameData.setTurbo(false);
            turbo = new Turbo("gib gas lieber michael");
        }else if (gameData.hasTurbo() && gameData.isLastLap()) {
            long piecesToGo = gameData.getRace().getTrack().getPieces().size() - currentPiece.getIndex();
            if (piecesToGo < 4) {
                gameData.setTurbo(false);
                turbo = new Turbo("gib gas lieber michael");
            }
        }
        return Optional.fromNullable(turbo);
    }


    private SendMsg calcSwitch() {
        SwitchLane switchCommand = null;
        Piece nextPiece = gameData.getNextPiece();
        double inPieceDistance = gameData.getInPieceDistance();
        if (nextPiece.isSwitchLane() &&
                inPieceDistance > 0 && inPieceDistance < 10 &&
                !gameData.isCarOnShortestLane()) {

            if (gameData.switchLeft()) {
                switchCommand = SwitchLane.createLeft();
            } else {
                switchCommand = SwitchLane.createRight();
            }
        }
        return switchCommand;
    }
}
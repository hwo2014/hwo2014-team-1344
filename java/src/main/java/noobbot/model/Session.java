package noobbot.model;

public class Session {
    private long laps;
    private long maxLapTimeMs;
    private boolean quickRace;

    public long getLaps() {
        return laps;
    }

    public void setLaps(long laps) {
        this.laps = laps;
    }

    public long getMaxLapTimeMs() {
        return maxLapTimeMs;
    }

    public void setMaxLapTimeMs(long maxLapTimeMs) {
        this.maxLapTimeMs = maxLapTimeMs;
    }

    public boolean isQuickRace() {
        return quickRace;
    }

    public void setQuickRace(boolean quickRace) {
        this.quickRace = quickRace;
    }
}

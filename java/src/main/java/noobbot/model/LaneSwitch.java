package noobbot.model;

/**
 * Created by jonas on 18.04.14.
 */
public class LaneSwitch {
    private long startLaneIndex;
    private long endLaneIndex;

    public long getEndLaneIndex() {
        return endLaneIndex;
    }

    public void setEndLaneIndex(long endLaneIndex) {
        this.endLaneIndex = endLaneIndex;
    }

    public long getStartLaneIndex() {
        return startLaneIndex;
    }

    public void setStartLaneIndex(long startLaneIndex) {
        this.startLaneIndex = startLaneIndex;
    }

    @Override
    public String toString() {
        return startLaneIndex + "->" +endLaneIndex;
    }

    public boolean isSwitch(){
        return startLaneIndex!=endLaneIndex;
    }
}

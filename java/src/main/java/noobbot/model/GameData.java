package noobbot.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by jonas on 16.04.14.
 */
public class GameData {
    private Race race;

    private List<CarPosition> lastPositions;
    private List<CarPosition> currentPositions;

    private long gameTick;

    private TrackProfile trackProfile;
    private boolean turbo;

    public void init(){
        for (int i = 0; i < race.getTrack().getPieces().size(); i++) {
            race.getTrack().getPieces().get(i).setIndex(i);
        }
    }

    public TrackProfile getTrackProfile() {
        if (trackProfile == null) {
            trackProfile = new TrackProfile(getRace().getTrack());
        }
        return  trackProfile;
    }

    private double currentSpeed = 0;

    public Race getRace() {
        return race;
    }

    public void setTuroAvailable(boolean turbo) {
        this.turbo = turbo;
    }

    public void markMyCar(CarId id) {
        Objects.requireNonNull(id);

        for (Car c : race.getCars()) {
            c.setMycar(c.getId().getColor().equals(id.getColor()) &&
                    c.getId().getName().equals(id.getName()));
        }
    }

    public Lane getMyLane(){
        LaneSwitch carLane = getMyCurrentPosition().getPiecePosition().getLaneSwitch();
        Long myCurrentLaneIndex = carLane.getStartLaneIndex();
        for (Lane l : getRace().getTrack().getLanes()){
            if(l.getIndex()==myCurrentLaneIndex){
                return l;
            }
        }
        throw new IllegalStateException("no lane?");
    }

    public boolean isCarOnShortestLane(){
        List<Lane> lanes = getRace().getTrack().getLanesSorted();
        Long myCurrentLaneIndex = getMyLane().getIndex();

        if (moreLeftThanRightBends()) {
            long index = lanes.get(0).getIndex();
            return Long.valueOf(index).equals(myCurrentLaneIndex);
        }
        long index = lanes.get(lanes.size() - 1).getIndex();
        return Long.valueOf(index).equals(myCurrentLaneIndex);
    }

    private boolean moreLeftThanRightBends() {
        TrackProfile profile = getTrackProfile();

        int currentPieceIndex = (int) getMyCurrentPosition().getPiecePosition().getPieceIndex();
        int nextSwitchIndex = profile.getNextSwitch(currentPieceIndex).getIndex();

        Piece nextBend = profile.getNextBend(currentPieceIndex);
        Piece nextnextBend = profile.getNextBend(nextBend.getIndex());
        Piece nextnextnextBend = profile.getNextBend(nextnextBend.getIndex());

        int countLeft = nextBend.getAngle()<0?1:0;
        int countRight = nextBend.getAngle()>0?1:0;

        if(nextSwitchIndex>nextnextBend.getIndex()){
            countLeft+= nextnextBend.getAngle()<0?1:0;
            countRight += nextnextBend.getAngle()>0?1:0;
        }

        if(nextSwitchIndex>nextnextnextBend.getIndex()){
            countLeft+= nextnextnextBend.getAngle()<0?1:0;
            countRight += nextnextnextBend.getAngle()>0?1:0;
        }

        return countLeft>countRight;
    }

    public boolean switchLeft(){
        return moreLeftThanRightBends();
    }


    public Piece nextBendPiece() {
        CarPosition myCurrentPosition = getMyCurrentPosition();
        Piece nextBend = getTrackProfile().getNextBend(myCurrentPosition.getPieceIndex());
        return nextBend;
    }


    public double calcAcceleration(){
        double lastSpeed = getMyLastPosition().getSpeed();
        double currentSpeed = getMyCurrentPosition().getSpeed();
        return currentSpeed - lastSpeed;
    }

    public double calcAngleDiff() {
        double lastAngle = Math.abs(getMyLastPosition().getAngle());
        double currentAngle = Math.abs(getMyCurrentPosition().getAngle());
        return currentAngle - lastAngle;
    }

    public double calcCurrentSpeed() {

        double inPieceDistanceLastTurn = getMyLastPosition().getPiecePosition().getInPieceDistance();
        double inPieceDistance = getMyCurrentPosition().getPiecePosition().getInPieceDistance();

        if (inPieceDistanceLastTurn < inPieceDistance) {
            currentSpeed = inPieceDistance - inPieceDistanceLastTurn;
        }

        getMyCurrentPosition().setSpeed(currentSpeed);
        return currentSpeed;
    }


    public Piece getNextPiece(int refIndex){
        List<Piece> pieces = getRace().getTrack().getPieces();
        int size = pieces.size();

        int nextIndex = (refIndex + 1) % size;

        return pieces.get(nextIndex);
    }


    public Piece getLastPiece(){
        return getLastPiece(1);
    }

    public Piece getLastPiece(int refIndex){
        int currentPieceIndex = (int) getMyCurrentPosition().getPiecePosition().getPieceIndex();

        int index = currentPieceIndex-refIndex;
        if(index>=0){
            return getRace().getTrack().getPieces().get(index);
        }

        int size = getRace().getTrack().getPieces().size();

        int lastIndex = size + currentPieceIndex - refIndex;

        return getRace().getTrack().getPieces().get(lastIndex);

    }

    public Piece getNextPiece() {
        int pieceIndex = (int) getMyCurrentPosition().getPiecePosition().getPieceIndex();
        return getNextPiece(pieceIndex);
    }

    public Car getMyCar() {
        for (Car c : race.getCars()) {
            if (c.isMycar())
                return c;
        }
        throw new RuntimeException("where is my car");
    }

    public CarPosition getMyLastPosition() {
        for (CarPosition pos : lastPositions) {
            CarId myCarId = getMyCar().getId();
            CarId carId = pos.getId();
            if (carId.equals(myCarId)) {
                return pos;
            }
        }
        return getMyCurrentPosition();
    }

    public CarPosition getMyCurrentPosition() {
        for (CarPosition pos : getCurrentPositions()) {
            CarId myCarId = getMyCar().getId();
            CarId carId = pos.getId();
            if (carId.equals(myCarId)) {
                return pos;
            }
        }
        throw new IllegalStateException("where is my position");
    }

    public List<CarPosition> getCurrentPositions() {
        return currentPositions;
    }

    public void setCurrentPositions(List<CarPosition> currentPositions) {
        Objects.requireNonNull(currentPositions);
        if (this.lastPositions == null) {
            this.lastPositions = new ArrayList<>();
        }
        if (this.currentPositions != null) {
            this.lastPositions = this.currentPositions;
        }
        this.currentPositions = currentPositions;
    }

    public Piece getCurrentPiece() {
        CarPosition myPosition = getMyCurrentPosition();
        return getRace().getTrack().getPieces().get((int) myPosition.getPiecePosition().getPieceIndex());
    }

    public double getInPieceDistance() {
        return getMyCurrentPosition().getPiecePosition().getInPieceDistance();
    }

    public double calcCurrentSectionLength() {
        TrackItem trackItem = getCurrentTrackItem();
        return trackItem.calcTrackItemLength();
    }

    public double calcCurrentSectionPositionPercentage(){
        double sectionLength = calcCurrentSectionLength();
        double sectionPosition = calcCurrentSectionPosition();
        return 100*sectionPosition/sectionLength;
    }

    public double calcCurrentSectionPosition(){
        TrackItem trackItem = getCurrentTrackItem();

        Piece currentPiece = getCurrentPiece();

        PiecePosition piecePosition = getMyCurrentPosition().getPiecePosition();
        double inPieceDistance = piecePosition.getInPieceDistance();

        for (Piece p :trackItem.getPieces()){
            if(p.getIndex()==currentPiece.getIndex()){
                break;
            }
            inPieceDistance+= p.getLength();
        }

        if(inPieceDistance<0){
            throw new IllegalStateException();
        }
        return inPieceDistance;
    }

    private TrackItem getCurrentTrackItem() {
        Piece currentPiece = getCurrentPiece();
        return getTrackProfile().getTrackItemBy(currentPiece.getIndex());
    }


    public int calcNextBendDistance() {
        Piece bendPiece = nextBendPiece();
        int nextbendPiece = bendPiece.getIndex();

        TrackItem bend = getTrackProfile().getTrackItemBy(nextbendPiece);

        int currentPieceIndex = getMyCurrentPosition().getPieceIndex();
        int nextBendPieceIndex = bend.getStartIndex();

        if(currentPieceIndex>nextBendPieceIndex){
            int distance = getRace().getTrack().getPieces().size() + nextBendPieceIndex - currentPieceIndex;
            return distance;
        }

        return nextBendPieceIndex-currentPieceIndex;
    }


    public boolean hasTurbo() {
        return turbo;
    }

    public void setTurbo(boolean turbo) {
        this.turbo = turbo;
    }

    public long getGameTick(){
       return gameTick;
    }

    public void setGameTick(long gameTick) {
        this.gameTick = gameTick;
    }

    public boolean isLastLap() {
        return getMyCurrentPosition().getLap()== getRace().getRaceSession().getLaps()-1;
    }
}

package noobbot.model;

/**
 * Created by jonas on 18.04.14.
 */
public class CarPosition {
    private CarId id;
    private double angle;

    private PiecePosition piecePosition;
    private double speed;

    public int getPieceIndex(){
        return (int)piecePosition.getPieceIndex();
    }

    public long getLap() {
        return getPiecePosition().getLap();
    }

    public PiecePosition getPiecePosition() {
        return piecePosition;
    }

    public void setPiecePosition(PiecePosition piecePosition) {
        this.piecePosition = piecePosition;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public CarId getId() {
        return id;
    }

    public void setId(CarId id) {
        this.id = id;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

}

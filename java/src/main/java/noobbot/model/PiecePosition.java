package noobbot.model;

/**
 * Created by jonas on 18.04.14.
 */
public class PiecePosition {
    private long pieceIndex;
    private double inPieceDistance;
    private LaneSwitch lane;
    private long lap;

    public double getInPieceDistance() {
        return inPieceDistance;
    }

    public void setInPieceDistance(double inPieceDistance) {
        this.inPieceDistance = inPieceDistance;
    }

    public LaneSwitch getLaneSwitch() {
        return lane;
    }

    public void setLaneSwitch(LaneSwitch lane) {
        this.lane = lane;
    }

    public long getPieceIndex() {
        return pieceIndex;
    }

    public void setPieceIndex(long pieceIndex) {
        this.pieceIndex = pieceIndex;
    }

    public long getLap() {
        return lap;
    }

    public void setLap(long lap) {
        this.lap = lap;
    }
}

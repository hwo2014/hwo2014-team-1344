package noobbot.model;

import java.util.Objects;

public class CarId {
    private String name;
    private String color;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


    @Override
    public boolean equals(Object obj) {
        CarId other = (CarId) obj;
        return Objects.equals(name, other.name) && Objects.equals(color, other.getColor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, color);
    }
}

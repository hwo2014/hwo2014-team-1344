package noobbot.model;

import java.util.ArrayList;
import java.util.List;

public class TrackItem {
    private final int startIndex;
    private int endIndex;
    private List<Piece> pieces;

    TrackItem(int startIndex, int endIndex, Piece piece) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.pieces = new ArrayList<>();
        this.pieces.add(piece);
    }

    public double calcTrackItemLength(){
        double length = 0;
        for (Piece p : pieces) {
            length += p.getLength();
        }
        return Math.abs(length);
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setEndIndex(int endIndex){
        this.endIndex = endIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public Piece getFirstPiece(){
        return pieces.get(0);
    }
    public Piece getLastPiece(){
        return pieces.get(pieces.size()-1);
    }

    public List<Piece> getPieces() {
        return pieces;
    }
}

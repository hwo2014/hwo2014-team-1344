package noobbot.model;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Created by jonas on 26.04.14.
 */
public class TrackProfile {
    private LinkedList<TrackItem> items;
    private Track track;


    public TrackProfile(Track track) {
        this.track = Objects.requireNonNull(track);
        this.items = new LinkedList<>();
        calcTrackProfile();
    }

    public TrackItem getTrackItemBy(int pieceIndex){
        for (TrackItem t: items){
            for (Piece p: t.getPieces()){
              if( p.getIndex() == pieceIndex ){
                  return t;
              }
            }
        }

        throw new IllegalStateException();
    }

    private void calcTrackProfile() {
        for (int i = 0; i < track.getPieces().size(); i++) {
            Piece p = track.getPieces().get(i);
            if (items.isEmpty()) {
                items.add(createItem(i, p));
            }

            TrackItem lastItem = items.get(items.size() - 1);

            TrackItem currentItem = createItem(i, p);

            if(lastItem.getLastPiece().getAngle()!=currentItem.getLastPiece().getAngle()){
                addItem(lastItem, currentItem);
            }

            if(p.getAngle()==0&&p.isSwitchLane()){
                addItem(lastItem, currentItem);
            }

            if(lastItem.getLastPiece().getAngle()==currentItem.getLastPiece().getAngle()){
                lastItem.getPieces().add(p);
            }
        }
    }

    private void addItem(TrackItem lastItem, TrackItem currentItem) {
        lastItem.setEndIndex(lastItem.getStartIndex()+lastItem.getPieces().size());
        items.add(currentItem);
    }

    private TrackItem createItem(int i, Piece p) {
        return new TrackItem(i, i, p);
    }

    public Piece getNextSwitch(final int pieceIndex) {
        List<TrackItem> switches = getSwitches();
        return getNextTrackItem(pieceIndex, switches);
    }

    public Piece getNextBend(final int pieceIndex) {
        List<TrackItem> bends = getBends();
        return getNextTrackItem(pieceIndex, bends);
    }

    private Piece getNextTrackItem(final int pieceIndex, List<TrackItem> items) {
        Optional<TrackItem> tryFind = Iterables.tryFind(items, new Predicate<TrackItem>() {
            @Override
            public boolean apply(TrackItem input) {
                return input.getEndIndex() > pieceIndex && input.getStartIndex() > pieceIndex;
            }
        });

        if(tryFind.isPresent()){
            return tryFind.get().getLastPiece();
        }

        return items.get(0).getLastPiece();
    }

    private List<TrackItem> getSwitches(){
        Iterable<TrackItem> c = Iterables.filter(items, new Predicate<TrackItem>() {
            @Override
            public boolean apply(TrackItem input) {
                return input.getLastPiece().isSwitchLane();
            }
        });
        return Lists.newArrayList(c);
    }

    private List<TrackItem> getBends() {
        Iterable<TrackItem> c = Iterables.filter(items, new Predicate<TrackItem>() {
            @Override
            public boolean apply(TrackItem input) {
                return input.getLastPiece().isBend();
            }
        });
        return Lists.newArrayList(c);
    }

}



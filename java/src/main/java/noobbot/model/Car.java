package noobbot.model;

public class Car {
    private boolean mycar;
    private CarId id;
    private CarSpec dimensions;

    public CarId getId() {
        return id;
    }

    public void setId(CarId id) {
        this.id = id;
    }

    public CarSpec getDimensions() {
        return dimensions;
    }

    public void setDimensions(CarSpec dimensions) {
        this.dimensions = dimensions;
    }

    public boolean isMycar() {
        return mycar;
    }

    public void setMycar(boolean mycar) {
        this.mycar = mycar;
    }
}

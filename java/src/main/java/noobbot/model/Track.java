package noobbot.model;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Track {
    private String id;
    private String name;
    private List<Piece> pieces;
    private List<Lane> lanes;
    private Point startingPoint;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Piece> getPieces() {
        return pieces;
    }

    public void setPieces(List<Piece> pieces) {
        this.pieces = pieces;
    }

    public List<Lane> getLanes() {
        return lanes;
    }

    public void setLanes(List<Lane> lanes) {
        this.lanes = lanes;
    }

    public Point getStartingPoint() {
        return startingPoint;
    }

    public void setStartingPoint(Point startingPoint) {
        this.startingPoint = startingPoint;
    }

    public List<Lane> getLanesSorted() {
        Collections.sort(lanes, new Comparator<Lane>() {
            @Override
            public int compare(Lane o1, Lane o2) {
                return Double.valueOf(o1.getDistanceFromCenter()).compareTo(o2.getDistanceFromCenter());
            }
        });
        return lanes;
    }
}

package noobbot.model;

/**
 * Created by jonas on 27.04.14.
 */
public enum BendCategory {
    VERY_HARD, HARD,SOFT,VERY_SOFT;
}

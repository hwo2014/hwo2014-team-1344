package noobbot.model;

import com.google.gson.annotations.SerializedName;

public class Piece {
    private double length;
    @SerializedName("switch")
    private boolean switchLane;
    private double angle;
    private double radius;
    private int index;

    public double getLength() {
        if(length==0){
            return 3.14 * radius * Math.abs(angle) / 180;
        }
        return length;
    }

    public double getLength(Lane lane) {
        double distanceFromCenter = lane.getDistanceFromCenter();
        if(length==0){
            return 3.14 * radius+distanceFromCenter * angle / 180;
        }
        return length;
    }

    public double getMinLength(Track track) {
        Lane lane = track.getLanesSorted().get(0);

        double distanceFromCenter = lane.getDistanceFromCenter();
        if(length==0){
            return 3.14 * radius+distanceFromCenter * angle / 180;
        }
        return length;
    }

    public boolean isBend(){
        return angle!=0;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public boolean isSwitchLane() {
        return switchLane;
    }

    public void setSwitchLane(boolean switchLane) {
        this.switchLane = switchLane;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }


    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}

package noobbot.model;

/**
 * Created by jonas on 16.04.14.
 */
public class CarSpec {
    private double length;
    private double width;
    private double guideFlagPosition;

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getGuideFlagPosition() {
        return guideFlagPosition;
    }

    public void setGuideFlagPosition(double guideFlagPosition) {
        this.guideFlagPosition = guideFlagPosition;
    }
}
